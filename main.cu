#include <iostream>

__global__
void f(){
    printf("Hello cuda!\n");
}


int main(int argc, char** argv){
    f<<<1,1>>>();
    cudaDeviceSynchronize();
    return 0;
}